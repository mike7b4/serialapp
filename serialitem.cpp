#include <QDebug>
#include "serialitem.h"

QVariant DataLine::data(int column, int role) const
{
    switch (role)
    {
        case Qt::EditRole:
            return false;
        case Qt::DisplayRole:
        case Text:
        qDebug () << "col" << column;
        if (column == 0)
        {
            return text;
        }
        if (column == 1)
        {
            return hex;
        }
        else if (column == 2)
        {
            return timeStamp.toString();
        }
        break;
        case Hex:
            return hex;
        case TimeStamp:
            return timeStamp.toString();
        case Color:
            if (isOk)
                return QColor("green");
            else if (isError)
                return QColor("red");
            else if (isWarning)
                return QColor("magenta");
            else
                return QColor("white");
        case Qt::SizeHintRole:
            return QVariant();
        case Style:
            return QFont::StyleNormal;
    }

    return QVariant(QString("Role not set %1").arg(role));
}
