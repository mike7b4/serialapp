#include <qdebug.h>
#include "serialportfilter.h"
#include "serialthread.h"

SerialThread::SerialThread(QObject *parent, SerialPortFilter *serial) :
    QThread(parent),
  pSerial(serial)
{
}

void SerialThread::run()
{
    while (1)
    {
        if (pSerial->waitForReadyRead(1))
        {
            pSerial->onReadyRead();
        }
//        usleep(500);
#if 0
        QString incoming = pSerial->readAll();
        if (incoming.length())
        {
            qDebug() << incoming;
        }
        else{
            usleep(500);
        }
#endif
    }
}
