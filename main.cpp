#include "mainwindow.h"
#include <QApplication>
#include <signal.h>
#include <stdio.h>

QApplication *g_app = (QApplication *)NULL;
QMainWindow *g_main_window = (MainWindow *)NULL;
extern "C"
{
    void on_signal(int sig)
    {
        switch(sig)
        {
            case SIGSEGV:
                printf("Segmentation fault\n");
                g_app->quit();
                delete g_main_window;
                delete g_app;
                signal(sig, SIG_DFL);
                exit(-1);
            break;
            case SIGINT:
                printf("terminate <ctrl>C\n");
            break;
            case SIGTERM:
                printf("terminate\n");
                break;
        }
        g_app->quit();
       // signal(sig, SIG_DFL);
    }
}
int main(int argc, char *argv[])
{
    int res;
    g_app = new QApplication(argc, argv);
    g_main_window = new MainWindow();
    if (!g_app || !g_main_window)
    {
        if (g_app)
        {
            delete g_app;
        }
        exit(-1);
        return -1;
    }
    signal(SIGINT, on_signal);
    signal(SIGTERM, on_signal);
    signal(SIGSEGV, on_signal);
    g_main_window->show();
    res = g_app->exec();
    delete g_main_window;
    delete g_app;
    printf("I am the first\n");
    return res;
}
