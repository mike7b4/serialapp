#include <QRegExp>
#include <QDebug>
#include <QDateTime>
#include "serialportfilter.h"
#include "serialthread.h"
SerialPortFilter::SerialPortFilter(QObject *parent) :
    QSerialPort(parent),
    incoming(),
    stopWhenBytes("\r\n"),
    clearWhenBytes(),
    rxTimeoutAfterMSec(0),
    showLineFeedAsText(false),
    whenError(),
    whenWarning(),
    thread(this, this)
{
    connect(this, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
}

SerialPortFilter::~SerialPortFilter()
{
    if (isOpen())
    {
        qDebug() << "close serial";
        close();
    }
}

bool SerialPortFilter::open()
{
    bool res;
    res = QSerialPort::open(QIODevice::ReadWrite);
    if (res)
    {
       //thread.start();
    }
    return res;
}

void SerialPortFilter::onReadyRead()
{
    int i = 0;
    int stoplength;
    QByteArray array;
    incoming += readAll();
    if (incoming.length() == 0)
    {
        return ;
    }
//    qDebug() <<  << incoming;
    /* check if clearwhen is set and if incoming text holds the text */
    if (!clearWhenBytes.isEmpty() &&
        (i = incoming.indexOf(clearWhenBytes)) != -1)
    {
        /* get rid of everything BEFORE clearWhenBytes */
        qDebug() << "clearpos" << i;
        incoming = incoming.mid(i);
        emit clearWhen();
    }

    stoplength = stopWhenBytes.length();
    if (stoplength)
    {
        /* array will be used as tmp later */
        while((i = incoming.indexOf(stopWhenBytes)) != -1)
        {
            array = incoming.left(i+stoplength);
            incoming = incoming.mid(i+stoplength);
            doLine(array);
         };

        if (rxTimeoutAfterMSec > 0)
        {
            timer.stop();
            timer.setInterval(rxTimeoutAfterMSec);
            timer.start();
        }
    }
    else if (rxTimeoutAfterMSec > 0)
    {
        /* no point */
    }
    else
    {
        doLine(incoming);
        incoming.clear();
    }
}

void SerialPortFilter::setReceiveTimeout(int timeout)
{
     rxTimeoutAfterMSec = timeout;
     timer.stop();
     if (timeout > 0)
     {
         timer.setInterval(rxTimeoutAfterMSec);
         timer.start();
     }
}
void SerialPortFilter::doLine(QByteArray array)
{
    QRegExp rx("[a-zA-Z!\"#¤%&/();:'\^~\*´̣_-<>]*");
    QString str(array);
    if (rx.indexIn(str) != -1)
    {
        bool iserror = whenError.isEmpty() ? false : array.contains(whenError);
        bool iswarning = whenWarning.isEmpty() ? false : array.contains(whenWarning);
        bool isok = whenOk.isEmpty() ? false : array.contains(whenOk);
        if (showLineFeedAsText)
        {
            str = str.replace("\r", "\\r").replace("\n", "\\n");
        }
        else
        {
            str = str.replace("\r", "").replace("\n", "");
        }

        DataLine dline(str, array.toHex().toUpper(), array.length(), iserror, iswarning, isok);
        emit newLineReceived(dline);
    }
    else
    {
        DataLine dline("", array.toHex().toUpper(), array.length(), array.contains(whenError), array.contains(whenWarning), array.contains(whenOk));
        emit newLineReceived(dline);
    }
}

void SerialPortFilter::onTimeout()
{
    if (incoming.length())
    {
        doLine(incoming);
        incoming.clear();
    }
}
