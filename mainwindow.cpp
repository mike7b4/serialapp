
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QDateTime>
#include <QFileDialog>
#include <QMessageBox>
#include <qdebug.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    pFile(NULL),
    timerRefreshPorts(),
    settings()
{
    ui->setupUi(this);
    setupUI();
    connect(&timerRefreshPorts, SIGNAL(timeout()), this, SLOT(iteratePorts()));
}


MainWindow::~MainWindow()
{
    settings.setPort(ui->comboBoxPort->currentText());
    settings.setDatabits(ui->comboBoxDataBits->currentText().toInt());
    settings.setBaudrate(ui->comboBoxBaudrate->currentText().toInt());
    settings.setStopbits(ui->comboBoxStopbits->currentText().toInt());
    settings.setValue("Serial/StopBytes", ui->entryIncomingLineEnd->text());
    settings.setValue("Serial/ClearIfInBytes", ui->entryClearIfInBytes->text());
    settings.setValue("Serial/FilterInTimeout", ui->filterInTimeout->value());
    settings.setValue("Serial/FilterInBinary", ui->radioBinary->isChecked());
    settings.setValue("Serial/ShowLineFeedAsText", serialPort.getShowLineFeedAsText());
    settings.setValue("UI/InAutoscroll", ui->checkBoxInAutoscroll->isChecked());
    settings.setValue("Serial/WhenError", ui->entryWhenError->text());
    settings.setValue("Serial/WhenWarning", ui->entryWhenWarning->text());
    settings.setValue("Serial/WhenOk", ui->entryWhenOk->text());
    settings.setValue("Settings/LgActive", ui->checkboxLogToFile->checkState());
    settings.setValue("Serial/Parity", ui->comboParity->currentIndex());
    settings.setBackBufferMax(ui->spinMaxBuffer->value());
    if (pFile)
    {
        delete pFile;
    }

    delete ui;
}

void MainWindow::setupUI()
{
    ui->comboBoxBaudrate->addItem(QIcon(), "9600");
    ui->comboBoxBaudrate->addItem(QIcon(), "19200");
    ui->comboBoxBaudrate->addItem(QIcon(), "38400");
    ui->comboBoxBaudrate->addItem(QIcon(), "57600");
    ui->comboBoxBaudrate->addItem(QIcon(), "115200");
    ui->comboBoxBaudrate->addItem(QIcon(), "230400");
    ui->comboBoxBaudrate->addItem(QIcon(), "460800");
    ui->comboBoxBaudrate->addItem(QIcon(), "576000");
    ui->comboBoxBaudrate->addItem(QIcon(), "921600");
    ui->comboBoxBaudrate->setCurrentText(QString("%1").arg(settings.getBaudrate()));

    ui->comboBoxDataBits->addItem(QIcon(), "5");
    ui->comboBoxDataBits->addItem(QIcon(), "6");
    ui->comboBoxDataBits->addItem(QIcon(), "7");
    ui->comboBoxDataBits->addItem(QIcon(), "8");
    ui->comboBoxDataBits->setCurrentText(QString("%1").arg(settings.getDatabits()));//QString("%1").arg(settings.getDatabits()));

    ui->comboParity->addItem(QIcon(), "None");
    ui->comboParity->addItem(QIcon(), "Even");
    ui->comboParity->addItem(QIcon(), "Odd");
    ui->comboParity->addItem(QIcon(), "Space");
    ui->comboParity->addItem(QIcon(), "Mark");
    ui->comboParity->addItem(QIcon(), "Uknown");
    ui->comboParity->setCurrentIndex(settings.value("Serial/Parity", 0).toInt());

    ui->comboBoxStopbits->addItem(QIcon(), "1");
    ui->comboBoxStopbits->addItem(QIcon(), "2");
    ui->comboBoxStopbits->setCurrentText(QString("%1").arg(settings.getStopbits()));

    ui->comboBoxHandshake->addItem(QIcon(), "None");
    ui->comboBoxHandshake->addItem(QIcon(), "Software");
    ui->comboBoxHandshake->addItem(QIcon(), "Hardware (CTS/RTS)");
    ui->comboBoxHandshake->setCurrentIndex(0);
    ui->pushButtonClose->setVisible(false);
    ui->groupBoxSend->setVisible(false);

    iteratePorts();
    timerRefreshPorts.start(1000);

    ui->entryIncomingLineEnd->setText(settings.value("Serial/StopBytes").toString());
    ui->filterInTimeout->setValue(settings.value("Serial/FilterInTimeout").toInt());
    if (settings.value("Serial/FilterInBinary").toBool() == true)
    {
        ui->radioBinary->setChecked(true);
        ui->radioRegexp->setChecked(false);
    }
    else
    {
        ui->radioBinary->setChecked(false);
        ui->radioRegexp->setChecked(true);
    }
    ui->entryClearIfInBytes->setText(settings.value("Serial/ClearIfInBytes").toString());
    ui->comboBoxSendAsType->addItem(QIcon(), "append: \\n", "\n");
    ui->comboBoxSendAsType->addItem(QIcon(), "append: \\r", "\r");
    ui->comboBoxSendAsType->addItem(QIcon(), "append: \\r", "\r\n");
    ui->comboBoxSendAsType->addItem(QIcon(), "append: \\0", "\0");
    ui->comboBoxSendAsType->addItem(QIcon(), "input is binary", "binary");

    ui->treeWidgetIncoming->setColumnCount(3);
    ui->treeWidgetIncoming->setRootIsDecorated(false);
    ui->treeWidgetIncoming->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents	);
    ui->treeWidgetIncoming->header()->setSectionResizeMode(1, QHeaderView::ResizeToContents	);
    ui->treeWidgetIncoming->header()->setSectionResizeMode(2, QHeaderView::ResizeToContents	);
    ui->treeWidgetIncoming->headerItem()->setText(0, ("Timestamp"));
    ui->treeWidgetIncoming->headerItem()->setText(1, ("Size"));
    ui->treeWidgetIncoming->headerItem()->setText(2, ("Received data"));
    ui->treeWidgetOutgoing->setColumnCount(3);
    ui->treeWidgetOutgoing->setRootIsDecorated(false);
    ui->treeWidgetOutgoing->header()->setSectionResizeMode(1, QHeaderView::ResizeToContents	);
    ui->treeWidgetOutgoing->header()->setSectionResizeMode(2, QHeaderView::ResizeToContents	);
    ui->treeWidgetOutgoing->headerItem()->setText(0, ("Timestamp"));
    ui->treeWidgetOutgoing->headerItem()->setText(1, ("Size"));
    ui->treeWidgetOutgoing->headerItem()->setText(2, ("Sent data"));

    ui->treeWidgetOutgoing->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents	);
    ui->treeWidgetOutgoing->header()->setSectionResizeMode(1, QHeaderView::ResizeToContents	);
    ui->treeWidgetOutgoing->header()->setSectionResizeMode(2, QHeaderView::ResizeToContents	);

    ui->checkBoxInAutoscroll->setChecked(settings.value("UI/InAutoscroll", true).toBool());
    ui->checkboxLogToFile->setChecked(settings.value("Serial/LogActive", true).toBool());
    on_checkboxLogToFile_clicked();

    ui->spinMaxBuffer->setValue(settings.getBackBufferMax());

    ui->entryWhenError->setText(settings.value("Serial/WhenError").toString());
    ui->entryWhenWarning->setText(settings.value("Serial/WhenWarning").toString());
    ui->entryWhenOk->setText(settings.value("Serial/WhenOk").toString());

    serialPort.setError(ui->entryWhenError->text().toUtf8());
    serialPort.setOk(ui->entryWhenOk->text().toUtf8());
    serialPort.setWarning(ui->entryWhenWarning->text().toUtf8());

    connect(&serialPort, SIGNAL(newLineReceived(DataLine)), this, SLOT(onNewLineReceived(DataLine)));
    connect(&serialPort, SIGNAL(clearWhen()), ui->treeWidgetIncoming, SLOT(clear()));
}

void MainWindow::on_pushButtonOpen_clicked()
{
    serialPort.setPortName(ui->comboBoxPort->currentText());
    serialPort.setReadBufferSize(5);
    if (serialPort.open())
    {
        timerRefreshPorts.stop();
        // for seome wierd reason this has to be set after open
        serialPort.setBaudRate(ui->comboBoxBaudrate->currentText().toInt());
        ui->treeWidgetIncoming->clear();
        ui->comboBoxPort->setEnabled(false);
        ui->pushButtonOpen->setVisible(false);
        ui->pushButtonClose->setVisible(true);
        ui->groupBoxSend->setVisible(true);
        ui->groupBoxOutgoing->setVisible(true);
        ui->entrySendData->setFocus();
    }
}

void MainWindow::iteratePorts(void)
{
    int i = -1;
    ui->comboBoxPort->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
       QSerialPort serial;
       serial.setPort(info);
       if (serial.open(QIODevice::ReadWrite))
       {
           serial.close();
           i++;
           ui->comboBoxPort->addItem(QIcon(), info.portName(), info.portName());
           if (settings.getPort() == info.portName())
           {
               ui->comboBoxPort->setCurrentIndex(i);
           }
       }
    }
}

void MainWindow::on_pushButtonClose_clicked()
{
    timerRefreshPorts.start(1000);
    serialPort.close();
    ui->comboBoxPort->setEnabled(true);
    ui->pushButtonOpen->setVisible(true);
    ui->pushButtonClose->setVisible(false);
    ui->groupBoxSend->setVisible(false);
    ui->groupBoxOutgoing->setVisible(false);
}

void MainWindow::on_comboBoxBaudrate_currentIndexChanged(const QString &rate)
{
    serialPort.setBaudRate(rate.toInt());
}

void MainWindow::on_comboBoxDataBits_currentIndexChanged(const QString &bits)
{
    /* the enum is mapped as 5 .. 8 so  this cast is safe */
    serialPort.setDataBits((QSerialPort::DataBits)bits.toInt());
}

void MainWindow::on_comboBoxStopbits_currentIndexChanged(const QString &sbits)
{
    /* the enum is mapped as 1 .. 2 so  this cast is safe */
    serialPort.setStopBits((QSerialPort::StopBits)sbits.toInt());
}

void MainWindow::on_entrySendData_returnPressed()
{
    on_buttonSend_clicked();
}

void MainWindow::on_buttonSend_clicked()
{
    QString date = QDateTime::currentDateTime().toString("hh:mm:ss.z");
    QTreeWidgetItem *item = new QTreeWidgetItem();
    QByteArray array;
    if (ui->comboBoxSendAsType->itemData(ui->comboBoxSendAsType->currentIndex()).toString() == "binary")
    {
        QRegExp nrx("[g-zG-ZåäöÅÄÖ!\"#¤%&/();:'\^~\*´̣_-<>]*");
        /* get rid of chars */
        QString data = ui->entrySendData->text();
        data = data.replace(nrx, "");
        array = QByteArray::fromHex(data.toUtf8());
        item->setText(2, array.toHex().toUpper());
        serialPort.write(array);
        ui->entrySendData->setText("");
    }
    else
    {
        QTreeWidgetItem *child = new QTreeWidgetItem();
        item->addChild(child);
        array.append(QByteArray(ui->entrySendData->text().toUtf8()));
        array.append(QByteArray(ui->comboBoxSendAsType->itemData(ui->comboBoxSendAsType->currentIndex()).toString().toUtf8()));
        item->setText(2, QString(array).replace("\r", "").replace("\n", ""));
        child->setText(0, array.toHex().toUpper());
        serialPort.write(array);
        ui->entrySendData->setText("");
    }
    item->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);

    item->setText(0, date);
    item->setText(1, QString("%1").arg(array.length()));
    ui->treeWidgetOutgoing->addTopLevelItem(item);
    ui->treeWidgetOutgoing->scrollToBottom();
}

void MainWindow::on_entrySendData_textChanged(const QString &data)
{
    QString s = data;
    qDebug() << "parse: " << data;
    /* atleast match : and one digit or hex before we switch to binarymode */
    QRegExp rx("^:[\\da-fA-F][\\da-fA-F:]*");
    if (ui->comboBoxSendAsType->itemData(ui->comboBoxSendAsType->currentIndex()).toString() == "binary")
    {
        QRegExp nrx("[g-zG-ZåäöÅÄÖ!\"#¤%&/())'\^~\*´̣_;<>-]*");
        ui->entrySendData->setText(s.replace(nrx, ""));
    }
    else if(rx.exactMatch(data))
    {
        ui->comboBoxSendAsType->setCurrentIndex(4);
    }


    //   qDebug() <<(int) rx.indexIn(data);
  //  qDebug() <<(int) rx.exactMatch(data);
  // qDebug() << s.replace(nrx, "");
}

void MainWindow::on_buttonClearIncoming_clicked()
{
    ui->treeWidgetIncoming->clear();
}

void MainWindow::on_comboBoxBaudrate_currentIndexChanged(int index)
{

}


void MainWindow::onNewLineReceived(DataLine dline)
{
   // QString date = QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
    int back = settings.getBackBufferMax();
    QByteArray array;
    QTreeWidgetItem *item;
    array = serialPort.readAll();

    if (back > 0 && back == ui->treeWidgetIncoming->topLevelItemCount())
    {
        item = ui->treeWidgetIncoming->topLevelItem(0);
        ui->treeWidgetIncoming->removeItemWidget(item, 0);
        delete item;
    }

    item = new QTreeWidgetItem();
    if (dline.text.length())
    {
        QTreeWidgetItem *child = new QTreeWidgetItem();
        if (dline.isError)
        {
            item->setTextColor(1, QColor("red"));
            item->setTextColor(2, QColor("red"));
        }
        if (dline.isOk)
        {
            item->setTextColor(1, QColor("green"));
            item->setTextColor(2, QColor("green"));
        }
        else if (dline.isWarning)
        {
            item->setTextColor(1, QColor("magenta"));
            item->setTextColor(2, QColor("magenta"));
        }

        item->setText(0, dline.date);
        item->setText(2, dline.text);
        item->setText(1, QString("%1").arg(dline.length));
        child->setText(2, dline.hex);
        child->setTextColor(2, QColor("blue"));
        item->addChild(child);
        if (pFile)
        {
            QTextStream stream(pFile);
            stream << dline.date << "\t" << dline.text << "\n";
        }
    }
    else
    {
        item->setText(0, dline.date);
        item->setText(2, dline.hex);
        item->setText(1, QString("%1").arg(dline.length));
        item->setTextColor(1, QColor("blue"));
        item->setTextColor(2, QColor("blue"));
        if (pFile)
        {
            QTextStream stream(pFile);
            stream << dline.date << "\t" << dline.hex << "\n";
        }
    }


    ui->treeWidgetIncoming->addTopLevelItem(item);
    if (ui->checkBoxInAutoscroll->isChecked())
    {
        ui->treeWidgetIncoming->scrollToBottom();
    }
}

void MainWindow::on_comboBoxPort_currentIndexChanged(const QString &port)
{
}

void MainWindow::on_entryIncomingLineEnd_textChanged(const QString &in)
{
    QString text(in);
    if (ui->radioBinary->isChecked())
    {
        serialPort.setStopWhenBytes(QByteArray::fromHex(text.toUtf8()));
    }
    else
    {
        text = text.replace("\\r", "\r").replace("\\n", "\n");
        qDebug() << text;
        serialPort.setStopWhenBytes(QByteArray(text.toUtf8()));
    }
}

void MainWindow::on_entryClearIfInBytes_textChanged(const QString &when)
{
    QString text(when);
    if (ui->radioBinary->isChecked())
    {
        serialPort.setClearWhenBytes(QByteArray::fromHex(text.toUtf8()));
    }
    else
    {
       text = text.replace("\\r", "\r").replace("\\n", "\n");
       serialPort.setClearWhenBytes(QByteArray(text.toUtf8()));
    }
}

void MainWindow::on_checkBoxInAutoscroll_clicked()
{
    if (ui->checkBoxInAutoscroll->isChecked())
    {
        ui->treeWidgetIncoming->scrollToBottom();
    }
}

void MainWindow::on_entryWhenWarning_editingFinished()
{
    qDebug() << "finished";
    serialPort.setWarning(ui->entryWhenWarning->text().toUtf8());
}

void MainWindow::on_entryWhenError_editingFinished()
{
    qDebug() << "finished";
    serialPort.setError(ui->entryWhenError->text().toUtf8());
}


void MainWindow::on_treeWidgetOutgoing_activated(const QModelIndex &index)
{
    ui->entrySendData->setText(ui->treeWidgetOutgoing->model()->data(index).toString());
    ui->entrySendData->setFocus();
}

void MainWindow::on_filterInTimeout_valueChanged(int timeout)
{
    serialPort.setReceiveTimeout(timeout);
}

void MainWindow::on_entryWhenOk_editingFinished()
{
    serialPort.setOk(ui->entryWhenOk->text().toUtf8());
}

void MainWindow::on_pushButton_clicked()
{
    QString fn = QFileDialog::getSaveFileName(this, tr("Save as..."),
                                                  QString(), tr("TXT files (*.txt);"));
    if (!fn.isEmpty())
    {
        if (pFile)
        {
            delete pFile;
            pFile = NULL;
        }
        pFile = new QFile(fn);
        if (!pFile->open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text))
        {
            QMessageBox msg;
            msg.setText(tr("Error"));
            msg.setInformativeText(tr("open of file '%1' failed").arg(fn));
            ui->checkboxLogToFile->setChecked(false);
        }
        else
        {
            settings.setValue("Settings/LogFile", fn);
            ui->checkboxLogToFile->setChecked(true);
        }
    }
}

void MainWindow::on_checkboxLogToFile_clicked()
{
    if (!ui->checkboxLogToFile->checkState())
    {
        if (pFile)
        {
            pFile->close();
            delete pFile;
            pFile = NULL;
        }
    }
    else
    {
        QString fn = settings.value("Settings/LogFile", "").toString();
        qDebug() << "d" << fn;
        if (!fn.isEmpty())
        {
            pFile = new QFile(fn);
            if (!pFile->open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
            {
                delete pFile;
                pFile = NULL;
                ui->checkboxLogToFile->setChecked(false);
                qDebug() << "open log '" << fn << "' failed";
            }
            else
            {
                qDebug() << "log opened";
            }
        }
        else
        {
            qDebug() << "No logfile specified";
            ui->checkboxLogToFile->setChecked(false);
        }
    }
}

void MainWindow::on_spinMaxBuffer_editingFinished()
{
    settings.setBackBufferMax(ui->spinMaxBuffer->value());
}

void MainWindow::on_comboParity_currentIndexChanged(int index)
{
    qDebug() << index;
    serialPort.setParity((QSerialPort::Parity)index);
}

void MainWindow::on_comboBoxPort_highlighted(const QString &arg1)
{
    // turn off timer when user highligts object in combo
    if (timerRefreshPorts.isActive())
    {
        timerRefreshPorts.stop();
    }
}

void MainWindow::on_comboBoxPort_activated(const QString &arg1)
{
    // turn on timer when user select object in combo
    settings.setPort(ui->comboBoxPort->currentText());
    timerRefreshPorts.start(1000);
}

void MainWindow::on_treeWidgetOutgoing_doubleClicked(const QModelIndex &index)
{
    on_buttonSend_clicked();
}
