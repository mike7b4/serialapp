#-------------------------------------------------
#
# Project created by QtCreator 2013-07-28T20:13:21
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = serialapp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    serialportfilter.cpp \
    serialappsettings.cpp \
    serialthread.cpp

HEADERS  += mainwindow.h \
    serialportfilter.h \
    serialappsettings.h \
    appconfig.h \
    serialthread.h

FORMS    += mainwindow.ui
