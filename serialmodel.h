#ifndef SERIALMODEL_H
#define SERIALMODEL_H
#include <QObject>
#include <QAbstractItemModel>
#include <QList>
#include "serialitem.h"
class SerialModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit SerialModel(QObject *parent = NULL);
    ~SerialModel();
    QVariant data(int row, const QByteArray &stringRole) const;
    QVariant data(const QModelIndex &index, int i) const;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    void append(DataLine dline);
    QHash<int, QByteArray> roleNames() const;
    bool hasChildren(const QModelIndex &parent) const { return parent.isValid() ? false: true; }
private:
    QList<DataLine*> rootItem;
};

#endif // SERIALMODEL_H
