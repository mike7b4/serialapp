#include <QDebug>
#include "serialitem.h"
#include "serialmodel.h"

namespace {
    QHash<QByteArray, int> roleMapping;
}


SerialModel::SerialModel(QObject *parent)
    : QAbstractItemModel(parent),
      rootItem()
{
}

SerialModel::~SerialModel()
{
    foreach (DataLine *ptr, rootItem)
    {
        delete ptr;
    }
}

QHash<int, QByteArray> SerialModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Text] = "label";
    roles[Hex] = "hex";
    roles[TimeStamp] = "timestamp";
    roles[Color] = "color";
    if (roleMapping.isEmpty()) {
        QHash<int, QByteArray>::ConstIterator it = roles.constBegin();
        for (;it != roles.constEnd(); ++it)
            roleMapping.insert(it.value(), it.key());

    }
    return roles;
}

QModelIndex SerialModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    DataLine *childItem = rootItem.at(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

void SerialModel::append(DataLine dline)
{
    beginInsertRows(QModelIndex(), rootItem.count(), rootItem.count() + 1);
    rootItem.append(new DataLine(dline));
    endInsertRows();
}


int SerialModel::rowCount(const QModelIndex &) const
{
    return rootItem.count();
}

int SerialModel::columnCount(const QModelIndex &) const
{
    return 3;
}

QModelIndex SerialModel::parent(const QModelIndex &) const
{
    return QModelIndex();
}

QVariant SerialModel::data(int row, const QByteArray &stringRole) const
{
    qDebug() <<  "freaked" << stringRole;
    QHash<QByteArray, int>::ConstIterator it = roleMapping.constFind(stringRole);

    if (it == roleMapping.constEnd())
        return QVariant();

    return data(index(row, 0), *it);
}

QVariant SerialModel::data(const QModelIndex &index, int role) const
{
    qDebug() <<  "freakeds" << role << ":" << index.row();
    if (index.row() < 0 || index.row() >= rootItem.count()) {
        qWarning() << "Attempted to access out of range row: " << index.row();
        return QVariant();
    }

    QVariant foo=rootItem.at(index.row())->data(index.column(), role);
    return foo;
}


