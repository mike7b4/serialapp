#include "appconfig.h"
#include "serialappsettings.h"
SerialAppSettings::SerialAppSettings() :
    QSettings("7b4", APP_NAME)
{
}

SerialAppSettings::~SerialAppSettings()
{
}

void SerialAppSettings::setPort(QString iport)
{
    setValue("Serial/port", iport);
}

void SerialAppSettings::setBaudrate(int baudrate)
{
    setValue("Serial/baudrate", baudrate);
}

void SerialAppSettings::setDatabits(int databits)
{
    setValue("Serial/databits", databits);
}

void SerialAppSettings::setStopbits(int stopbits)
{
    setValue("Serial/stopbits", stopbits);
}

/** \brief How many chars allowed in scrollview
    \param max allowed chars
*/
void SerialAppSettings::setBackBufferMax(int max)
{
    setValue("ListView/MaxBuffer", max);
}

