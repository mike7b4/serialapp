#ifndef SERIALITEM_H
#define SERIALITEM_H
#include <QString>
#include <QColor>
#include <QFont>
#include <QDateTime>
#include <QVariant>

enum DataLineRoles
{
    Text = Qt::UserRole, //QString
    Hex, // QString
    TimeStamp, // QString
    Color, // QColor
    Style
};
class DataLine
{
    public:
        QString text;
        QString hex;
        QDateTime timeStamp;
        int length;
        bool isError;
        bool isWarning;
        bool isOk;
        DataLine(QString ascii, QString _hex, int _length, bool iserror, bool iswarning, bool isok):
        text(ascii),
        hex(_hex),
        timeStamp(QDateTime::currentDateTime()),
        length(_length),
        isError(iserror),
        isWarning(iswarning),
        isOk(isok)
        {
        };
        DataLine(const DataLine& dline):
        text(dline.text),
        hex(dline.hex),
        timeStamp(dline.timeStamp),
        length(dline.length),
        isError(dline.isError),
        isWarning(dline.isWarning),
        isOk(dline.isOk)
        {
        };

        QVariant data(int column, int role) const;
};

#endif // SERIALITEM_H
