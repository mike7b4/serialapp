#ifndef APPCONFIG_H
#define APPCONFIG_H
#include <qglobal.h>
#define APP_NAME "SerialApp"

/* most computers today has no phy serialport, so we take for granted box has some kind of USB -> serial adaptor */
#ifdef Q_OS_LINUX
/* on linux
   /dev/ttyUSBx means USBtoRS232 adapter
   /dev/ttyACMx  modems or BT RFCOM links?? atleast on my Lenovo X230 laptop
   /dev/ttySx physical comport
*/
#define DEFAULT_SERIAL_PORT "/dev/ttyUSB0"
#elif Q_OS_BSD
#define DEFAULT_SERIAL_PORT "/dev/ttyU0"
#elif WIN32
/* dont have a clue how windows enumerates comports its seems to be randomnly depending and RFCOM or serial adapters also use COMxx. */
#define DEFAULT_SERIAL_PORT "COM1"
#endif

#define APP_NAME "SerialApp"
#define APP_VERSION "0.0.1"

#endif // APPCONFIG_H
