import QtQuick 2.1
import QtQuick.Window 2.1
import QtQuick.Controls 1.0
import SerialModel 1.0
Rectangle {
    color: "black"
    width: 600
    height: 800

    Timer{
        id: timer
        interval: 1000
        running: true
        onTriggered: { console.log("U fail");  view.model = incoming;  }
    }
    Component
    {
        id: serial
        Text {
            width: parent.width
            height: 30
            text: model.text
            color: model.color
        }
    }
    ListView{
        id: view
        anchors.fill: parent
        delegate: serial
      //  model: incoming
    }
}
