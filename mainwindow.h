#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QTreeWidgetItem>
#include <QMainWindow>
#include <QFile>
#include "serialportfilter.h"
#include "serialappsettings.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QFile *pFile;
    QTimer timerRefreshPorts;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    SerialAppSettings settings;
    SerialPortFilter serialPort;
    void setupUI(void);
private slots:
    void iteratePorts(void);

    void on_pushButtonOpen_clicked();

    void on_pushButtonClose_clicked();

    void on_comboBoxBaudrate_currentIndexChanged(const QString &arg1);

    void on_comboBoxDataBits_currentIndexChanged(const QString &arg1);

    void on_comboBoxStopbits_currentIndexChanged(const QString &arg1);

    void on_entrySendData_returnPressed();

    void on_buttonSend_clicked();

    void on_entrySendData_textChanged(const QString &arg1);

    void on_buttonClearIncoming_clicked();

    void on_comboBoxBaudrate_currentIndexChanged(int index);

    void onNewLineReceived(DataLine gline);
    void on_comboBoxPort_currentIndexChanged(const QString &arg1);

    void on_entryIncomingLineEnd_textChanged(const QString &arg1);

    void on_entryClearIfInBytes_textChanged(const QString &arg1);

    void on_checkBoxInAutoscroll_clicked();

    void on_entryWhenWarning_editingFinished();

    void on_entryWhenError_editingFinished();

    void on_treeWidgetOutgoing_activated(const QModelIndex &index);

    void on_filterInTimeout_valueChanged(int arg1);

    void on_entryWhenOk_editingFinished();

    void on_pushButton_clicked();

    void on_checkboxLogToFile_clicked();

    void on_spinMaxBuffer_editingFinished();

    void on_comboParity_currentIndexChanged(int index);

    void on_comboBoxPort_highlighted(const QString &arg1);

    void on_comboBoxPort_activated(const QString &arg1);

    void on_treeWidgetOutgoing_doubleClicked(const QModelIndex &index);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
