#ifndef SERIALPORTFILTER_H
#define SERIALPORTFILTER_H

#include <QSerialPort>
#include <QTimer>
#include <QDateTime>
#include "serialthread.h"

class DataLine
{
    public:
        QString text;
        QString hex;
        int length;
        bool isError;
        bool isWarning;
        bool isOk;
        QString date;
        DataLine(QString ascii, QString _hex, int _length, bool iserror, bool iswarning, bool isok):
        text(ascii),
        hex(_hex),
        length(_length),
        isError(iserror),
        isWarning(iswarning),
        isOk(isok)
        {
            date = QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
        };
};

class SerialPortFilter;

class SerialThread : public QThread
{
    Q_OBJECT
    SerialPortFilter *pSerial;
public:
    explicit SerialThread(QObject *parent, SerialPortFilter *serial);
    void run() Q_DECL_OVERRIDE;
signals:

public slots:


};

class SerialPortFilter : public QSerialPort
{
    Q_OBJECT
    /* data will be stored in this aray until stopByte(s) is found */
    QByteArray incoming;
    /* emit newLineReceived when stopByte(s) received */
    QByteArray stopWhenBytes;
    /* everythiung before clear when... will be cleared if UI has connected clear signal */
    QByteArray clearWhenBytes;
    QByteArray whenError;
    QByteArray whenWarning;
    QByteArray whenOk;
    QTimer timer;
    QTimer timerRead;
    int rxTimeoutAfterMSec;
    bool showLineFeedAsText;
    SerialThread thread;
public:
    explicit SerialPortFilter(QObject *parent = 0);
    ~SerialPortFilter();

    bool open();
    bool getShowLineFeedAsText(){ return showLineFeedAsText;};
private:
    void doLine(QByteArray array);
signals:
    /* tell  UI to update UI with content */
    void newLineReceived(DataLine line);
    /* tell ui to clear list/text */
    void clearWhen();
public slots:
    void onReadyRead();
    void onTimeout();
    void setStopWhenBytes(QByteArray arr){ stopWhenBytes = arr; };
    void setShowLineFeedAsText(bool show){ showLineFeedAsText = show; };
    void setReceiveTimeout(int timeout);
    void setClearWhenBytes(QByteArray arr){ clearWhenBytes = arr; };
    void setError(QByteArray error){ whenError = error;};
    void setWarning(QByteArray warning){ whenWarning = warning;};
    void setOk(QByteArray ok){ whenOk = ok;};
};

#endif // SERIALPORTFILTER_H
