#ifndef SERIALAPPSETTINGS_H
#define SERIALAPPSETTINGS_H

#include <QSettings>
#include "appconfig.h"
class SerialAppSettings : public QSettings
{
    Q_OBJECT

    Q_PROPERTY( QString port READ getPort WRITE setPort )
    Q_PROPERTY( int baudrate READ getBaudrate WRITE setBaudrate )
    Q_PROPERTY( int databits READ getDatabits WRITE setDatabits )
    Q_PROPERTY( int stopbits READ getStopbits WRITE setStopbits )
    Q_PROPERTY( int max READ getBackBufferMax WRITE setBackBufferMax)
public:
    explicit SerialAppSettings();
    ~SerialAppSettings();
    QString getPort() { return value("Serial/port", DEFAULT_SERIAL_PORT).toString();};
    int getBaudrate() { return value("Serial/baudrate", 115200).toInt();};
    int getDatabits() { return value("Serial/databits", 8).toInt();};
    int getStopbits() { return value("Serial/stopbits", 1).toInt(); };
    int getBackBufferMax() { return value("ListView/MaxBuffer").toInt();};
signals:
    
public slots:
    void setPort(QString iport);
    void setBaudrate(int _baudrate);
    void setDatabits(int _databits);
    void setStopbits(int stopbits);
    void setBackBufferMax(int max);
};

#endif // SERIALAPPSETTINGS_H
